FROM python:3.7.2
WORKDIR /epiImage
ADD . /epiImage
RUN pip install -r requirements.txt
CMD ["python", "epiImage.py"]