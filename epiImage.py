import numpy as np
import requests, shutil, os
import argparse
import errno
import sys
import cv2

parser = argparse.ArgumentParser()
parser.add_argument('--url', required = True, help='URL of video, that will be processed')
parser.add_argument('--dir', nargs='?', const='tmp/', default='default-dir', required = True, help='Path of output directory')
res = parser.parse_args()

(dirname, filename) = os.path.split(res.url)
r = requests.get(res.url, stream=True)
if r.status_code == 200:
	try:
		with open(res.dir + filename, 'wb') as f:
			r.raw.decode_content = True
			shutil.copyfileobj(r.raw, f)
	except IOError as error:
		if error.errno == errno.ENOENT:
			sys.exit('Directory not found!')
		else:
			raise

cap = cv2.VideoCapture(res.dir + filename)
width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH ))
height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

epiImages = []
numOfFrame = 0  
while(cap.isOpened()):
	ret, frame = cap.read()
	if frame is not None:
		for row in range(height):
			cropped_image = frame[row:(row+1),0:width]
			if numOfFrame == 0:
				epiImages.append(cropped_image)
			else:
				epiImages[row] = np.concatenate((epiImages[row],cropped_image),axis = 0)    
	else:
		break
	numOfFrame += 1

frame_video_size = (width,numOfFrame)
fourcc = cv2.VideoWriter_fourcc(*'mp4v')
out = cv2.VideoWriter(res.dir + 'epi_' + filename,fourcc, 30.0, frame_video_size)
for i in range(len(epiImages)):
	out.write(epiImages[i])

cap.release()
out.release()
cv2.destroyAllWindows()
